import React from 'react';
import emailjs from 'emailjs-com';
import Swal from 'sweetalert2'
import { Form, Button } from 'react-bootstrap'

export default function ContactUs() {

    function sendEmail(e) {
        e.preventDefault();

        emailjs.sendForm('service_xn1ptze', 'template_eg0gbko', e.target, 'user_bESYlAJlfoaWP6HUCm14j')
            .then((result) => {
                Swal.fire({
                    icon: 'success',
                    title: 'Email Sent',
                    text: 'I will view this ASAP!',
                })
            }, (error) => {
                console.log(error.text);
                Swal.fire({
                    icon: 'error',
                    title: 'Email Not Sent',
                    text: error.text,
                })
            });

    }

    return (
        <Form className="contact-form" onSubmit={sendEmail}>
            <Form.Group controlId="formGroupEmail">
                <Form.Label>Type Your Email address</Form.Label>
                <Form.Control className="input-contact" type="email" placeholder="Enter email" name="user_email" />
            </Form.Group>

            <Form.Group controlId="formGroupSubject">
                <Form.Label>Subject</Form.Label>
                <Form.Control className="input-contact" type="text" placeholder="Enter Subject" name="subject" />
            </Form.Group>

            <Form.Group controlId="formGroupMessage">
                <Form.Label>Message</Form.Label>
                <Form.Control className="input-contact" as="textarea" placeholder="Enter message" rows={3} name="message" />
            </Form.Group>


            <Button variant="dark" type="submit">
                Submit
            </Button>
        </Form>
    );
}


{/* <form className="contact-form" onSubmit={sendEmail}>
<Form.Group controlId="formGroupEmail">
<Form.Label>Type Your Email address</Form.Label>
<Form.Control type="email" placeholder="Enter email" name="user_email" />
</Form.Group>
<input type="hidden" name="contact_number" />
<label>Subject</label>
<input type="text" name="subject" />
<br />
<label>Email</label>
<input type="email" name="user_email" />
<br />
<label>Message</label>
<textarea name="message" />
<br />
<input type="submit" value="Send" />
</form>
);
} */}