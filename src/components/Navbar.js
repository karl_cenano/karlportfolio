import React from 'react'
import { Link, BrowserRouter as Router } from 'react-router-dom'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import { Nav } from 'react-bootstrap'
const Navbar = ({ props }) => {


    return (
        <Router>
            <nav className="navbar Nav" >
                <h1>
                    <Link to="/"><span><i className="fa fa-grav links ml-4 mt-4 nav-icon"></i></span></Link>
                </h1>
                <ul>
                    <li className="mt-2 mr-5 mt-4 nav-small"><AnchorLink href="#home"><span className="links">Home</span></AnchorLink></li>
                    <li className="mt-2 mr-5 mt-4 nav-small"><AnchorLink href="#projects"><span className="links">Projects</span></AnchorLink></li>
                    <li className="mt-2 mr-5 mt-4 nav-small"><AnchorLink href='#contact'><span className="links endlink">Contact</span></AnchorLink></li>
                </ul>
            </nav >
        </Router>
    )

}


export default Navbar


{/* <nav className="navbar Nav">
                <h1>
                    <Link to="/"><span><i className="fa fa-grav links ml-4 mt-4 nav-icon"></i></span></Link>
                </h1>
                <ul>
                    <li className="mt-2 mr-5 mt-4 nav-small"><AnchorLink href="#home"><span className="links">Home</span></AnchorLink></li>
                    <li className="mt-2 mr-5 mt-4 nav-small"><AnchorLink href="#projects"><span className="links">Projects</span></AnchorLink></li>
                    <li className="mt-2 mr-5 mt-4 nav-small"><AnchorLink href='#contact'><span className="links">Contact</span></AnchorLink></li>
                </ul>
            </nav> */}