import React, { useRef, useEffect, useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import { TweenMax, Power3, gsap } from 'gsap'
import { Container, Jumbotron, Row, Col, Button } from 'react-bootstrap';
import Navbar from './components/Navbar'
import { CustomSquare } from './components/SvgLight'
import Path, { Svg } from 'react-svg-path';
import proj1 from './img/proj1.PNG'
import proj2 from './img/proj2.PNG'
import proj3 from './img/proj3.PNG'
import Wave from 'react-wavify'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { SmoothProvider } from 'react-smooth-scrolling'
import ContactUs from './components/ContactUs'

function App() {
  gsap.registerPlugin(ScrollTrigger);



  let ref = useRef(null)
  const projectHead = useRef(null)
  const project1 = useRef(null)
  const project2 = useRef(null)
  const project3 = useRef(null)
  const contact = useRef(null)


  useEffect(() => {
    const animationObj = {
      duration: 0.8,
      y: -80,
      opacity: 0,
    };
    gsap.from(projectHead.current, {
      scrollTrigger: {
        trigger: projectHead.current,
        start: 'top 50%',

      },
      ...animationObj,
    });
  }, []);

  useEffect(() => {
    const animationObj = {
      duration: 0.8,
      x: -200,
      opacity: 0,
    };
    gsap.from(project1.current, {
      scrollTrigger: {
        trigger: project1.current,
        start: 'top 50%',

      },
      ...animationObj,
    });
  }, []);

  useEffect(() => {
    const animationObj = {
      duration: 0.8,
      x: 200,
      opacity: 0,
    };
    gsap.from(project2.current, {
      scrollTrigger: {
        trigger: project2.current,
        start: 'top 50%',

      },
      ...animationObj,
    });
  }, []);

  useEffect(() => {
    const animationObj = {
      duration: 0.8,
      x: -200,
      opacity: 0,
    };
    gsap.from(project3.current, {
      scrollTrigger: {
        trigger: project3.current,
        start: 'top 50%',


      },
      ...animationObj,
    });
  }, []);

  useEffect(() => {
    const animationObj = {
      duration: 1,
      y: -400,
      opacity: 0,
    };
    gsap.from(contact.current, {
      scrollTrigger: {
        trigger: contact.current,
        start: 'top 10%',

      },
      ...animationObj,
    });
  }, []);


  useEffect(() => {
    TweenMax.from('.name-col', 1.5,
      {
        x: -200,
        scaleX: 1,
        opacity: 0,
        height: '100vh',
        ease: "Circ.easeout"
      }
    )
  }, [])






  return (


    <div className="App" ref={ref}>

      <section id="home" className="page-1">
        <Navbar />
        <Jumbotron className="Jumbotron" fluid>
          <Container className="mt-5 landing-container">
            <Row className="">
              <Col className=" name-col">
                <h1 className=" landing-name" >HI! I'M <span className="text-orange">KARL</span></h1>
                <h3 className=" landing-desc"> I'M A<span className="text-orange"> FULL-STACK</span> WEB DEVELOPER</h3>
                <p className=" landing-desc"> Welcome to my portfolio. I'm a web developer with an engineering background. I take programming of any kind as my passion.</p>
              </Col>
              <div className="light-fix">
                <CustomSquare />
              </div>

            </Row>
          </Container>
        </Jumbotron>
      </section>

      <div className="wave-1">
        <Wave fill='#f79902'
          paused={false}
          options={{
            height: 20,
            amplitude: 40,
            speed: 0.15,
            points: 5
          }}

        />
      </div>

      <section id="projects" className="page-2">

        <h1 className="project-header" ref={projectHead}>Projects </h1>
        <Container fluid>
          <Row className=" row-proj1" ref={project1}>


            <Col>
              <div className="image"> <img className="image__img" src={proj1}></img></div>
              <div className="image__overlay">
                <div className="image__title">Budget App</div>
                <a className="image__description" href="https://capstone-3-client-enano.vercel.app/" target="_blank">Click here to view website</a>
              </div>
            </Col>
          </Row>
          <Row className=" row-proj1" ref={project2}>

            <Col >
              <div className="image"> <img className="image__img" src={proj2}></img></div>
              <div className="image__overlay">
                <div className="image__title">Social Media App</div>
                <a className="image__description" href="https://still-shelf-60479.herokuapp.com/" target="_blank">Click here to view website</a>
              </div>
            </Col>

          </Row>
          <Row className=" row-proj1" ref={project3}>
            <Col >
              <div className=""> <img className="image__img" src={proj3}></img></div>
              <div className="image__overlay">
                <div className="image__title">Booking Demo App</div>
                <a className="image__description" href="https://karl_cenano.gitlab.io/capstone2-enano/" target="_blank">Click here to view website</a>
              </div>
            </Col>

          </Row>

        </Container>
      </section>

      <section id="contact" className="page-3 " >


        <Container className="mt-5" >


          <Row className="row-contact " ref={contact}>

            <Col lg="6" className=""><h1 className="mb-5 mt-5">You Can Contact Me Here</h1><ContactUs /></Col>
            <Col lg="5" className="">
              <h1 className="social-head mt-5">or... Send A Message In</h1>
              <Button type="button" href="https://www.linkedin.com/in/karl-eñano-a77714123/" target="_blank" className="social-buttons1"><span><i className="fa fa-linkedin "></i></span>  LinkedIn</Button>

            </Col>
          </Row>

        </Container>



      </section>

      <footer className="footer">
        &copy; Karl Eñano 2021
      </footer>


    </div >


  );
}

export default App;
